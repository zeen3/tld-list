/// <reference types="node" />
import { Duplex, Readable } from 'stream';
import { IncomingHttpHeaders, IncomingHttpStatusHeader, ClientHttp2Stream } from 'http2';
export declare const tldm: RegExp;
export declare const tlds: Set<string>;
export declare const readtlds: (stream: Duplex | Readable) => void;
export declare const decompress: (headers: IncomingHttpHeaders & IncomingHttpStatusHeader, stream: ClientHttp2Stream) => Duplex;
export declare const filename = "tlds-alpha-by-domain.txt";
export declare const TLDList: Promise<Set<string>>;
declare const _default: Promise<Set<string>>;
export default _default;
//# sourceMappingURL=index.d.ts.map