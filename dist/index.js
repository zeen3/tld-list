"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const stream_1 = require("stream");
const http2_1 = require("http2");
const fs_1 = require("fs");
const zlib_1 = require("zlib");
const path_1 = require("path");
const readline_1 = require("readline");
exports.tldm = /^[A-Z0-9-]+$/;
exports.tlds = new Set();
exports.readtlds = (stream) => {
    readline_1.createInterface({ input: stream })
        .on('line', ln => exports.tldm.test(ln) ? exports.tlds.add(ln) : null);
};
exports.decompress = (headers, stream) => {
    switch (headers['content-encoding']) {
        case 'gzip': return stream.pipe(zlib_1.createGunzip());
        case 'deflate': return stream.pipe(zlib_1.createInflate());
        default: return stream.pipe(new stream_1.PassThrough());
    }
};
exports.filename = 'tlds-alpha-by-domain.txt';
exports.TLDList = new Promise((res, rej) => {
    const ETag = fs_1.promises.readFile(path_1.join(__dirname, `${exports.filename}.ETag`), 'utf8');
    http2_1.connect('https://data.iana.org').once('connect', async (session) => {
        const rq = session.request({
            ':path': '/TLD/tlds-alpha-by-domain.txt',
            'accept-encoding': 'deflate, gzip',
            'cache-control': 'max-age=0',
            'if-none-match': await ETag
        }).once('response', async (headers) => {
            if (Number(headers[':status'] || headers.status) === 304) {
                session.close();
            }
            else {
                await Promise.all([
                    new Promise(r => {
                        exports.decompress(headers, rq)
                            .pipe(fs_1.createWriteStream(path_1.join(__dirname, exports.filename))
                            .once('close', r));
                    }),
                    fs_1.promises.writeFile(path_1.join(__dirname, `${exports.filename}.ETag`), headers.etag, 'utf8')
                ]);
            }
            exports.readtlds(fs_1.createReadStream(path_1.join(__dirname, exports.filename)).once('close', () => res(exports.tlds)));
        });
    }).once('error', () => {
        try {
            exports.readtlds(fs_1.createReadStream(__dirname, exports.filename)
                .once('close', () => res(exports.tlds)));
        }
        catch (e) {
            rej(e);
        }
    });
});
exports.default = Promise.resolve(exports.TLDList);
//# sourceMappingURL=index.js.map