import { Duplex, Readable, PassThrough } from 'stream'
import { connect, IncomingHttpHeaders, IncomingHttpStatusHeader, ClientHttp2Stream } from 'http2'
import { createReadStream, createWriteStream, promises } from 'fs'
import { createGunzip, createInflate } from 'zlib'
import { join } from 'path'
import { createInterface } from 'readline'

export const tldm = /^[A-Z0-9-]+$/
export const tlds = new Set<string>()
export const readtlds = (stream:Duplex | Readable):void => {
	createInterface({input: stream})
		.on('line', ln => tldm.test(ln) ? tlds.add(ln) : null)
}
export const decompress = (headers: IncomingHttpHeaders & IncomingHttpStatusHeader, stream:ClientHttp2Stream):Duplex => {
	switch (headers['content-encoding']) {
		case 'gzip': return stream.pipe(createGunzip())
		case 'deflate': return stream.pipe(createInflate())
		default: return stream.pipe(new PassThrough())
	}
}
export const filename = 'tlds-alpha-by-domain.txt'
export const TLDList = new Promise((res:(tlds:Set<string>)=>void, rej) => {
	const ETag = promises.readFile(join(__dirname, `${filename}.ETag`), 'utf8')
	connect('https://data.iana.org').once('connect', async (session) => {
		const rq = session.request({
			':path': '/TLD/tlds-alpha-by-domain.txt',
			'accept-encoding': 'deflate, gzip',
			'cache-control': 'max-age=0',
			'if-none-match': await ETag
		}).once('response', async (headers:IncomingHttpHeaders & IncomingHttpStatusHeader) => {
			if (Number(headers[':status'] || headers.status) === 304) {
				session.close()
			} else {
				await Promise.all([
					new Promise(r => {
						decompress(headers, rq)
							.pipe(
								createWriteStream(join(__dirname, filename))
								.once('close', r)
							)
					}),
					promises.writeFile(join(__dirname, `${filename}.ETag`), headers.etag, 'utf8')
				])
			}
			readtlds(
				createReadStream(
					join(__dirname, filename)
				).once('close', () => res(tlds))
			)
		})
	}).once('error', () => {
		try {
			readtlds(
				createReadStream(__dirname, filename)
				.once('close', () => res(tlds))
			)
				
		} catch (e) {
			rej(e)
		}
	})
})
export default Promise.resolve(TLDList)
